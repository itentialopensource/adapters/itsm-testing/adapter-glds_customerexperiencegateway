
## 0.5.4 [10-15-2024]

* Changes made at 2024.10.14_20:19PM

See merge request itentialopensource/adapters/adapter-glds_customerexperiencegateway!13

---

## 0.5.3 [08-26-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-glds_customerexperiencegateway!11

---

## 0.5.2 [08-14-2024]

* Changes made at 2024.08.14_18:29PM

See merge request itentialopensource/adapters/adapter-glds_customerexperiencegateway!10

---

## 0.5.1 [08-07-2024]

* Changes made at 2024.08.06_19:42PM

See merge request itentialopensource/adapters/adapter-glds_customerexperiencegateway!9

---

## 0.5.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/itsm-testing/adapter-glds_customerexperiencegateway!8

---

## 0.4.4 [03-27-2024]

* Changes made at 2024.03.27_14:02PM

See merge request itentialopensource/adapters/itsm-testing/adapter-glds_customerexperiencegateway!7

---

## 0.4.3 [03-13-2024]

* Changes made at 2024.03.13_15:03PM

See merge request itentialopensource/adapters/itsm-testing/adapter-glds_customerexperiencegateway!6

---

## 0.4.2 [03-11-2024]

* Changes made at 2024.03.11_14:45PM

See merge request itentialopensource/adapters/itsm-testing/adapter-glds_customerexperiencegateway!5

---

## 0.4.1 [02-28-2024]

* Changes made at 2024.02.28_11:09AM

See merge request itentialopensource/adapters/itsm-testing/adapter-glds_customerexperiencegateway!4

---

## 0.4.0 [12-31-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/itsm-testing/adapter-glds_customerexperiencegateway!3

---

## 0.3.0 [05-25-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-glds_customerexperiencegateway!2

---

## 0.2.0 [10-22-2021]

- Switch return data flag for create outage task

See merge request itentialopensource/adapters/itsm-testing/adapter-glds_customerexperiencegateway!1

---

## 0.1.1 [10-14-2021]

- Initial Commit

See commit 8bdcbdf

---
