
## 0.3.0 [05-25-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-glds_customerexperiencegateway!2

---

## 0.2.0 [10-22-2021]

- Switch return data flag for create outage task

See merge request itentialopensource/adapters/itsm-testing/adapter-glds_customerexperiencegateway!1

---

## 0.1.1 [10-14-2021]

- Initial Commit

See commit 8bdcbdf

---
