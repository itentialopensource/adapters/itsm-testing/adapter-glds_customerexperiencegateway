# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the GLDSCustomerExperienceGateway System. The API that was used to build the adapter for GLDSCustomerExperienceGateway is usually available in the report directory of this adapter. The adapter utilizes the GLDSCustomerExperienceGateway API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The GLDS Customer Experience Gateway adapter from Itential is used to integrate the Itential Automation Platform (IAP) with GLDS Customer Experience Gateway to support various aspects of ITSM. With this adapter you have the ability to perform operations such as:

- Equipment
- Outages
- Subscriber
- Work Orders

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
