# GLDS Customer Experience Gateway

Vendor: GLDS
Homepage: https://www.glds.com/

Product: Customer Experience Gateway
Product Page: https://www.glds.com/

## Introduction
We classify GLDS Customer Experience Gateway into the ITSM (Service Management) domain as it provides the capabilities to manage service delivery, customer interaction, and service quality across different areas.

## Why Integrate
The GLDS Customer Experience Gateway adapter from Itential is used to integrate the Itential Automation Platform (IAP) with GLDS Customer Experience Gateway to support various aspects of ITSM. With this adapter you have the ability to perform operations such as:

- Equipment
- Outages
- Subscriber
- Work Orders

## Additional Product Documentation