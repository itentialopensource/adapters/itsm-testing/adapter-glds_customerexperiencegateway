/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-glds_customerexperiencegateway',
      type: 'GLDSCustomerExperienceGateway',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const GLDSCustomerExperienceGateway = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] GLDSCustomerExperienceGateway Adapter Test', () => {
  describe('GLDSCustomerExperienceGateway Class Tests', () => {
    const a = new GLDSCustomerExperienceGateway(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-glds_customerexperiencegateway-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-glds_customerexperiencegateway-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#subscriberLookup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.subscriberLookup(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriber', 'subscriberLookup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriberACCOUNT = 'fakedata';
    describe('#getSubscriber - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSubscriber(subscriberACCOUNT, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('123456203', data.response.Account);
                assert.equal('object', typeof data.response.SubscriberStatus);
                assert.equal('object', typeof data.response.SubscriberType);
                assert.equal('Roxanne Hunn', data.response.AccountName);
                assert.equal('Roxanne', data.response.FirstName);
                assert.equal('string', data.response.SecondName);
                assert.equal('Hunn', data.response.LastName);
                assert.equal(true, Array.isArray(data.response.PhoneNumbers));
                assert.equal('name@provider.com', data.response.BillingEmail);
                assert.equal('object', typeof data.response.ServiceAddress);
                assert.equal('object', typeof data.response.Franchise);
                assert.equal('object', typeof data.response.TimeZone);
                assert.equal('Pre-paid', data.response.PaymentPlan);
                assert.equal(true, Array.isArray(data.response.PaymentTypes));
                assert.equal('1975-03-09', data.response.DOB);
                assert.equal('RiskFactor', data.response.WinScoreRiskFactor);
                assert.equal(true, Array.isArray(data.response.ParentAccounts));
                assert.equal(true, Array.isArray(data.response.ChildAccounts));
                assert.equal('object', typeof data.response.SoftDisconnect);
                assert.equal(true, data.response.PINisSet);
                assert.equal(false, data.response.CanOrderPPV);
                assert.equal('object', typeof data.response.PPVDenyReason);
                assert.equal(true, data.response.InOutageArea);
                assert.equal(true, Array.isArray(data.response.ActiveOutages));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriber', 'getSubscriber', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBalance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBalance(subscriberACCOUNT, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Post-paid', data.response.PaymentPlan);
                assert.equal(13062.13, data.response.LastBillAfterPayments);
                assert.equal(19991.3, data.response.CurrentBalance);
                assert.equal(6929.17, data.response.NewActivity);
                assert.equal(256.37, data.response.CurrentAging30);
                assert.equal(249.72, data.response.CurrentAging60);
                assert.equal(236.37, data.response.CurrentAging90);
                assert.equal(12319.67, data.response.CurrentAging120);
                assert.equal(12319.67, data.response.PastDueAmt);
                assert.equal('2020-11-01', data.response.CurrentBillDueDate);
                assert.equal(14690.5, data.response.LastBillAmount);
                assert.equal('2020-10-30', data.response.LastBillDate);
                assert.equal(false, data.response.BillFormIsSet);
                assert.equal('2020-10-30', data.response.LastCycleCloseDate);
                assert.equal(99.99, data.response.LastPaymentAmount);
                assert.equal('2020-10-30', data.response.LastPaymentDate);
                assert.equal(1628.37, data.response.LatePaymentAmount);
                assert.equal(100, data.response.MonthlyPPVCreditLimit);
                assert.equal(10.1, data.response.AvailPPVCreditLimit);
                assert.equal('object', typeof data.response.PTP);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriber', 'getBalance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSubBucketBalances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSubBucketBalances(subscriberACCOUNT, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriber', 'getSubBucketBalances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSubscriberPackages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSubscriberPackages(subscriberACCOUNT, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriber', 'getSubscriberPackages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSIS - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSIS(subscriberACCOUNT, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.Packages));
                assert.equal(true, Array.isArray(data.response.Discounts));
                assert.equal(true, Array.isArray(data.response.TaxesAndFees));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriber', 'getSIS', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSubscriberPackageSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSubscriberPackageSummary(subscriberACCOUNT, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.RecurringCharges);
                assert.equal(4, data.response.RecurringDiscounts);
                assert.equal(10, data.response.TimedBilling);
                assert.equal(6, data.response.TimedPromoDiscounts);
                assert.equal(1, data.response.AfterDiscounts);
                assert.equal(7, data.response.TotalTaxed);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriber', 'getSubscriberPackageSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subscriberX = 555;
    describe('#getLastSubscriberTransactions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLastSubscriberTransactions(subscriberACCOUNT, subscriberX, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subscriber', 'getLastSubscriberTransactions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const equipmentACCOUNT = 'fakedata';
    const equipmentRefreshEquipmentBodyParam = [
      null
    ];
    describe('#refreshEquipment - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.refreshEquipment(equipmentACCOUNT, equipmentRefreshEquipmentBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-glds_customerexperiencegateway-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Equipment', 'refreshEquipment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const equipmentEQUIPMENTID = 'fakedata';
    const equipmentACTIONCODE = 'fakedata';
    describe('#performActionCode - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.performActionCode(equipmentEQUIPMENTID, equipmentACTIONCODE, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-glds_customerexperiencegateway-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Equipment', 'performActionCode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSubscriberEquipment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSubscriberEquipment(equipmentACCOUNT, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Equipment', 'getSubscriberEquipment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refreshAllEquipment - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.refreshAllEquipment(equipmentACCOUNT, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-glds_customerexperiencegateway-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Equipment', 'refreshAllEquipment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const outagesCreateOutageBodyParam = {
      OutageType: 'Planned',
      AffectedServiceClasses: [
        'INT'
      ],
      AffectedArea: [
        {
          Type: 'Subscriber',
          Values: [
            '000000100',
            '000013525'
          ]
        },
        {
          Type: 'Headend',
          Values: [
            '000',
            '001'
          ]
        },
        {
          Type: 'EquipmentID',
          Values: [
            '19195',
            '987654321000'
          ]
        },
        {
          Type: 'PhoneNumber',
          Values: [
            '17606021916',
            '17609671001'
          ]
        }
      ],
      StartTime: 'string',
      EstCloseTime: 'string'
    };
    describe('#createOutage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createOutage(outagesCreateOutageBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Outages', 'createOutage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const outagesOUTAGE = 555;
    const outagesUpdateOutageBodyParam = {
      OutageType: 'Planned',
      AffectedServiceClasses: [
        'INT'
      ],
      AffectedArea: [
        {
          Type: 'Subscriber',
          Values: [
            '000000100',
            '000013525'
          ]
        },
        {
          Type: 'Headend',
          Values: [
            '000',
            '001'
          ]
        },
        {
          Type: 'EquipmentID',
          Values: [
            '19195',
            '987654321000'
          ]
        },
        {
          Type: 'PhoneNumber',
          Values: [
            '17606021916',
            '17609671001'
          ]
        }
      ],
      StartTime: 'string',
      EstCloseTime: 'string'
    };
    describe('#updateOutage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateOutage(outagesOUTAGE, outagesUpdateOutageBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-glds_customerexperiencegateway-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Outages', 'updateOutage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const outagesACCOUNT = 'fakedata';
    const outagesX = 555;
    describe('#getLastOutages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLastOutages(outagesACCOUNT, outagesX, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Outages', 'getLastOutages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workOrdersACCOUNT = 'fakedata';
    const workOrdersX = 555;
    describe('#getLastWorkOrders - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLastWorkOrders(workOrdersACCOUNT, workOrdersX, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkOrders', 'getLastWorkOrders', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workOrdersWONUMBER = 'fakedata';
    describe('#getAvailability - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAvailability(workOrdersWONUMBER, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkOrders', 'getAvailability', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workOrdersReason = 'fakedata';
    describe('#cancelWorkOrder - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cancelWorkOrder(workOrdersWONUMBER, workOrdersReason, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-glds_customerexperiencegateway-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkOrders', 'cancelWorkOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workOrdersDATE = 'fakedata';
    const workOrdersPERIOD = 555;
    describe('#rescheduleWorkOrder - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rescheduleWorkOrder(workOrdersWONUMBER, workOrdersDATE, workOrdersPERIOD, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-glds_customerexperiencegateway-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkOrders', 'rescheduleWorkOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const homeStreetName = 'fakedata';
    const homeCity = 'fakedata';
    describe('#homeLookup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.homeLookup(null, homeStreetName, null, homeCity, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Home', 'homeLookup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const homeHOME = 555;
    describe('#getHome - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHome(homeHOME, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('721 Kimball Terrace San Ysidro CA 94636-9101', data.response.FullAddress);
                assert.equal('San Ysidro', data.response.city);
                assert.equal('CA', data.response.state);
                assert.equal('94636', data.response.ZIP);
                assert.equal('94636-9101', data.response.ZIPPlus4);
                assert.equal('060730176041014', data.response.BlockFIPS);
                assert.equal('object', typeof data.response.Franchise);
                assert.equal('object', typeof data.response.PropertyType);
                assert.equal('object', typeof data.response.HeadEnd);
                assert.equal('object', typeof data.response.Hub);
                assert.equal('object', typeof data.response.Node);
                assert.equal('object', typeof data.response.VoIPRateCenter);
                assert.equal('LM89546', data.response.DropTag);
                assert.equal(true, data.response.Wired);
                assert.equal(true, data.response.DropConnected);
                assert.equal('object', typeof data.response.TechRegion);
                assert.equal('object', typeof data.response.SalesArea);
                assert.equal('object', typeof data.response.ManagementArea);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Home', 'getHome', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const otherTYPE = 'fakedata';
    describe('#getReasons - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getReasons(otherTYPE, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Other', 'getReasons', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServiceClassList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServiceClassList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Other', 'getServiceClassList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelOutage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cancelOutage(outagesOUTAGE, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-glds_customerexperiencegateway-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Outages', 'cancelOutage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
